require "spec_helper"
require "./app/models/item"

describe Item do
  subject { Item.from_product_code(product_code) }

  context "for a known product code" do
    let(:product_code) { "FR1" }
    it "should return the right price" do
      expect(subject.price).to eq 3.11
    end
  end

  context "for an unknown product code" do
    let(:product_code) { "AT1" }

    it "should return the right price" do
      expect { subject.price }.to raise_error("Product Not Found")
    end
  end
end
