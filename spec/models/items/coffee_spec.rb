require "spec_helper"
require "./app/models/items/coffee"

describe Items::Coffee do
  subject { described_class.new }

  it "should have the right price" do
    expect(subject.price).to eq 11.23
  end

  it "should have the right name" do
    expect(subject.name).to eq "Coffee"
  end

  it "should have the right code" do
    expect(subject.code).to eq "CF1"
  end
end
