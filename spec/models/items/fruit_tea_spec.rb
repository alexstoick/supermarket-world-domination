require "spec_helper"
require "./app/models/items/fruit_tea"

describe Items::FruitTea do
  subject { described_class.new }

  it "should have the right price" do
    expect(subject.price).to eq 3.11
  end

  it "should have the right name" do
    expect(subject.name).to eq "Fruit tea"
  end

  it "should have the right code" do
    expect(subject.code).to eq "FR1"
  end
end
