require "spec_helper"
require "./app/models/items/apple"

describe Items::Apple do
  subject { described_class.new }

  it "should have the right price" do
    expect(subject.price).to eq 5
  end

  it "should have the right name" do
    expect(subject.name).to eq "Apple"
  end

  it "should have the right code" do
    expect(subject.code).to eq "AP1"
  end
end
