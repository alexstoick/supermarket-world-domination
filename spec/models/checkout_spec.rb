require "spec_helper"
require "./app/models/checkout"
require "./app/models/item"
require "./app/models/promotions/bulk_price_discount"
require "./app/models/promotions/bulk_free_item"

describe Checkout do
  subject { Checkout.new(pricing_rules) }
  let(:pricing_rules) { nil }

  let(:bulk_free_item_promo) do
    Promotions::BulkFreeItem.new({
      product_code: "AP1",
      required_quantity: 1,
      free_quantity: 1,
    })
  end

  let(:bulk_promo) do
    Promotions::BulkPriceDiscount.new({
      product_code: "CF1",
      min_quantity: 3,
      price_reduction: 0.50,
    })
  end

  context "#total_hash" do
    it "should have the right format" do
      expect(subject.total_hash).to eq({
        items_total: 0,
        promotion_total: 0,
        total: 0,
      })
    end
  end

  context "for no pricing rules" do
    let(:pricing_rules) { nil }
    let(:item_code) { "FR1" }

    context "#total" do
      context "for 1 item" do
        before do
          subject.scan(item_code)
        end

        it "has the right value" do
          expect(subject.total).to eq 3.11
        end
      end

      context "for 2 items" do
        before do
          subject.scan(item_code)
          subject.scan(item_code)
        end

        it "has the right value" do
          expect(subject.total).to eq 6.22
        end
      end
    end
  end

  context "#count_items_matching_code" do
    context "with one type of items" do
      before do
        subject.scan("FR1")
        subject.scan("FR1")
      end

      it "should have the right count" do
        expect(subject.count_items_matching_code("FR1")).to eq 2
      end
    end

    context "with more than 1 type of item" do
      before do
        subject.scan("FR1")
        subject.scan("AP1")
        subject.scan("CF1")
        subject.scan("CF1")
      end

      it "should have the right count for fruit tea" do
        expect(subject.count_items_matching_code("FR1")).to eq 1
      end

      it "should have the right count for coffee" do
        expect(subject.count_items_matching_code("CF1")).to eq 2
      end

      it "should have the right count for apple" do
        expect(subject.count_items_matching_code("AP1")).to eq 1
      end
    end
  end

  describe "with multiple promos active" do
    let(:pricing_rules) { [bulk_free_item_promo, bulk_promo] }

    it "should end up with the correct price" do
      subject.scan("AP1")
      expect(subject.total).to eq 5
      subject.scan("AP1")
      expect(subject.total).to eq 5
      subject.scan("FR1")
      expect(subject.total).to eq 8.11
      subject.scan("CF1")
      subject.scan("CF1")
      expect(subject.total).to eq 30.57
      subject.scan("CF1")
      expect(subject.total).to eq 40.30
    end
  end

  describe "with the bulk free item promo active" do
    let(:pricing_rules) { [bulk_free_item_promo] }

    context "when it matches" do
      before do
        subject.scan("AP1")
        subject.scan("AP1")
      end

      it "should reduce the total" do
        expect(subject.total).to eq 5
        subject.scan("AP1")
        expect(subject.total).to eq 10
      end
    end

    context "when it doesn't match" do
      before do
        subject.scan("AP1")
      end

      it "should not affect the total" do
        expect(subject.total).to eq 5
      end
    end

  end

  describe "with the bulk price promo active" do
    let(:pricing_rules) { [bulk_promo] }

    context "when it matches" do
      before do
        subject.scan("CF1")
        subject.scan("CF1")
      end

      it "should reduce the total" do
        expect(subject.total).to eq 22.46
        subject.scan("CF1")
        expect(subject.total).to eq 32.19
      end
    end

    context "when it doesn't match" do
      before do
        subject.scan("CF1")
      end

      it "should not affect the total" do
        expect(subject.total).to eq 11.23
      end
    end
  end
end
