require "spec_helper"
require "./app/models/promotions/bulk_free_item"
require "./app/models/checkout"

describe Promotions::BulkFreeItem, type: :model do

  subject { described_class.new(options) }
  let(:options) { {} }

  describe "Validations" do
    it { should validate_presence_of(:product_code) }

    it do
      should validate_numericality_of(:required_quantity)
    end

    it do
      should validate_numericality_of(:free_quantity)
    end
  end

  describe "#compute" do
    let(:options) do
      {
        product_code: product_code,
        required_quantity: 1,
        free_quantity: 1,
      }
    end
    let(:product_code) { "AP1" }
    let(:result) { subject.compute(checkout_double) }
    let(:checkout_double) { instance_double("Checkout") }

    before do
      allow(checkout_double).to(
        receive(:count_items_matching_code).
        with(product_code).
        and_return(item_count)
      )
    end

    context "when it's eligible more than once" do
      let(:item_count) { 4 }

      it "should return the price of two items" do
        expect(result).to eq 10
      end
    end

    context "when it's eligible just once" do
      let(:item_count) { 3 }

      it "should return the price of one item" do
        expect(result).to eq 5.0
      end
    end

    context "when it's not eligible" do
      let(:item_count) { 1 }

      it "should return 0" do
        expect(result).to eq 0
      end
    end
  end
end
