require "spec_helper"
require "./app/models/promotions/bulk_price_discount"
require "./app/models/checkout"

describe Promotions::BulkPriceDiscount, type: :model do

  subject { described_class.new(options) }
  let(:options) { {} }

  describe "Validations" do
    it { should validate_presence_of(:product_code) }

    it do
      should validate_numericality_of(:price_reduction)
    end

    it do
      should validate_numericality_of(:min_quantity)
    end
  end

  describe "#compute" do
    let(:options) do
      {
        product_code: product_code,
        min_quantity: 3,
        price_reduction: 0.50,
      }
    end
    let(:product_code) { "AP1" }
    let(:result) { subject.compute(checkout_double) }
    let(:checkout_double) { instance_double("Checkout") }

    before do
      allow(checkout_double).to(
        receive(:count_items_matching_code).
        with(product_code).
        and_return(item_count)
      )
    end

    context "when it's eligible" do
      let(:item_count) { 4 }

      it "should return the correct value" do
        expect(result).to eq 2
      end
    end

    context "when it's not eligible" do
      let(:item_count) { 2 }

      it "should return 0" do
        expect(result).to eq 0
      end
    end
  end
end
