# Main app
require "./app/models/checkout"
require "./app/models/promotions/bulk_price_discount"
require "./app/models/promotions/bulk_free_item"
require "./app/models/item"
require "sinatra"
require "pry"

set :bind, "0.0.0.0"
set :views, "./app/views"

pricing_rules = [
  Promotions::BulkPriceDiscount.new({
    product_code: "AP1",
    min_quantity: 3,
    price_reduction: 0.50,
  }),
  Promotions::BulkFreeItem.new({
    product_code: "CF1",
    required_quantity: 1,
    free_quantity: 1
  }),
]

co = Checkout.new(pricing_rules)

get '/' do
  @checkout = co
  erb :index
end

post '/scan' do
  co.scan(params["code"])
  @checkout = co
  erb :checkout
end
