module Items
  class FruitTea
    def name
      "Fruit tea"
    end

    def price
      3.11
    end

    def code
      "FR1"
    end
  end
end
