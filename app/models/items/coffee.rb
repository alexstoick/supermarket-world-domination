module Items
  class Coffee
    def name
      "Coffee"
    end

    def price
      11.23
    end

    def code
      "CF1"
    end
  end
end
