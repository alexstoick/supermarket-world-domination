module Items
  class Apple
    def name
      "Apple"
    end

    def price
      5.00
    end

    def code
      "AP1"
    end
  end
end
