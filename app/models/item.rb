require_relative "items/fruit_tea"
require_relative "items/apple"
require_relative "items/coffee"

class Item
  PRODUCTS = {
    "FR1" => Items::FruitTea,
    "AP1" => Items::Apple,
    "CF1" => Items::Coffee,
  }

  def self.from_product_code(product_code)
    class_name = PRODUCTS[product_code]
    raise "Product Not Found" if class_name.nil?

    class_name.new
  end
end
