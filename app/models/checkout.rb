class Checkout
  def initialize(pricing_rules)
    @pricing_rules = pricing_rules || []
    @items = []
  end

  def scan(product_code)
    @items << Item.from_product_code(product_code)
  end

  def count_items_matching_code(product_code)
    @items.count { |item| item.code == product_code }
  end

  def total
    items_total - promotion_total
  end

  def total_hash
    {
      items_total: items_total,
      promotion_total: promotion_total,
      total: total,
    }
  end

  attr_reader :items, :pricing_rules

  private

  def items_total
    items.map { |item| item.price }.inject(0,:+)
  end

  def promotion_total
    pricing_rules.inject(0) do |sum, promotion|
      sum + promotion.compute(self)
    end
  end
end
