require "active_model"
require "./app/models/item"

module Promotions
  class BulkFreeItem
    include ActiveModel::Validations

    validates_presence_of :product_code
    validates :required_quantity, numericality: true
    validates :free_quantity, numericality: true

    def initialize(options)
      @product_code = options[:product_code]
      @required_quantity = options[:required_quantity]
      @free_quantity = options[:free_quantity]
    end

    def compute(checkout)
      @checkout = checkout
      price = Item.from_product_code(product_code).price
      matching_items_count / (free_quantity+required_quantity)*free_quantity * price
    end

    attr_accessor :product_code, :free_quantity, :required_quantity

    private

    def matching_items_count
      @checkout.count_items_matching_code(product_code)
    end
  end
end
