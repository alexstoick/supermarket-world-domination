require "active_model"

module Promotions
  class BulkPriceDiscount
    include ActiveModel::Validations

    validates_presence_of :product_code
    validates :min_quantity, numericality: true
    validates :price_reduction, numericality: true

    def initialize(options)
      @product_code = options[:product_code]
      @min_quantity = options[:min_quantity]
      @price_reduction = options[:price_reduction]
    end

    def compute(checkout)
      @checkout = checkout
      return 0 unless eligible?
      matching_items_count * price_reduction
    end

    attr_accessor :product_code, :min_quantity, :price_reduction

    private

    def eligible?
      matching_items_count >= min_quantity
    end

    def matching_items_count
      @checkout.count_items_matching_code(product_code)
    end
  end
end
