console.log("lol")

$("#scan_button").on("click", function() {
  $("#scan_location").show();
});

$("#scan_form").on("submit", function(e) {
  e.preventDefault();
  $.ajax({
    url: "/scan",
    type: "POST",
    data: $(this).serialize(),
    success: function(data) {
      $("#checkout").html(data);
    },
  });
  $("#scan_location").hide();
});

