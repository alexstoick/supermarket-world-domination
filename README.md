## Global Domination Supermarket

Describing solution to the TextMaster challenge of running a supermarket that will take over the world.

---

## Approach

There is a `Checkout` model, and a model for every **item** (described in the challenge description), all living in the `Items` module. The `Checkout` model implements the required methods `scan` and `total`. There are also **2 promotions**, for the 2 types of discounts that can be applied as per the description of the challenge:
- `BulkPriceDiscount` - which reduces the amount each item costs if you bought at least a specified amount (buy X, each is $Y off)
- `BulkFreeItem` - which grants a number of free items for a given number of purchased items (buy X, get Y free)

---

I started out with a very naive implementation of the checkout class, which could `scan` items by their product code and save them, then return the item total. Once this was done, I refactored my initial approach to the `Item` class, by breaking them down into smaller classes, one for each product.

After I finished the refactor, I moved onto the creation of the **pricing rules**, which I have named _promotions_. I decided to make these very general (which was also hinted at by the challenge description), and I ended up creating two classes: `Promotions::BulkPriceDiscount` and `Promotions::BulkFreeItem` (_which might not be the greatest name!_). Both classes take one parameter for initialization, a **hash** which then is used to change the behaviour of the promotion and gives flexibility in terms of usage.

After I finished implementing the **pricing rules**, I thought: _"Hmm.. It all looks great, only if it was easier to interact with it."_
So I went and added few **barebones** views which display the current checkout, promotions and also includes an AJAX form which allows you to _scan_ more items into the checkout.

I have also added the ability to add more promotions in a separate branch(`feature/additional-promotions`) - which is functional; however it needs a bit more time, testing and JS love to get it to a _"user-safe"_ place (promotions have validations, which means you need to handle errors, and explain the problem to the user).

---

## App Installation & Start


- Clone repo: `git@bitbucket.org:alexstoick/supermarket-world-domination.git`
- Run bundle: `bundle install`

To boot the app: `ruby app.rb`

Navigate to http://localhost:4567
